#! /usr/bin/env python3
# -*- coding: UTF-8 -*-
"""
Multimonitor Infoscreen displaying status and emergency service related pages as well as maps.
Using WASTL (FF-Krems)
"""
import json
import logging
import multiprocessing
import os
import threading
import time
from datetime import datetime

from libinfoscreen import screen_unit, wastl_helper

# import cups

##########################################################################
# global CONFIG
##########################################################################


# CONFIG file
with open("/home/data/gitlab/ffp_infoscreen/config/ffp_infoscreen_config_dev.json") as config_fh:
    CONFIG = json.load(config_fh)

# logging
LOGFILE = "ffp_infoscreen_" + datetime.now().strftime(CONFIG["log"]["timestamp"]) + ".log"
LOGFILE_FULLPATH = os.path.join(CONFIG["log"]["path"], LOGFILE)

logging.basicConfig(level=logging.INFO,
                    filename=LOGFILE_FULLPATH,
                    format='%(asctime)s %(levelname)s: %(message)s')
# https://docs.python.org/2/library/logging.html?highlight=logging#integration-with-the-warnings-module
logging.captureWarnings(True)
# needs warning module?


################################################################################
# updater thread
################################################################################

def update_routine():
    """
    Update routine, fetch WASTL status periodically and forward information to screen processes.
    """

    # check if testdata file should be used to emulate emergency


    # 4 options for "info_source_to_use":
    # 1) wastl
    # 2) wastl_testdata
    # 4) other -> permanent alarm (and other text will lead to permanent alarm)

    # if testdata should be used, the desired source (wastl/blaulichtsms) needs
    # to be selected and "testdata" set to the filepath of the testdata

    if CONFIG["info_source"]["info_source_to_use"] == "wastl":
        alarm_code, wastl_msg = wastl_helper.check_wastl(CONFIG["info_source"]["wastl"]["cookie"])
        # check if WASTL status is valid and assign a code to the screen checker process
        # this way, future modifications to accept a different information source will be easy
        alarmcodes = ["b1", "b2", "b3", "b4", "s1", "s2", "s3", "t1", "t2", "t3"]
        if alarm_code.lower() in alarmcodes:
            logging.debug("valid WASTL alarm code: " + str(alarm_code))
            code_for_screen = "alarm"
        elif alarm_code == "normal":
            code_for_screen = "normal"
        else:
            logging.error("check_wastl() returned non valid alarmcode: " + str(alarm_code))
            code_for_screen = "error"
            FEEDBACK_Q.put("request_termination")

    elif CONFIG["info_source"]["info_source_to_use"] == "wastl_testdata":
        wastl_msg = json.load(open(CONFIG["info_source"]["testdata"]))
        alarm_code = wastl_msg["EinsatzData"][0]["Alarmstufe"]


    elif CONFIG["info_source"]["info_source_to_use"] == "blaulichtsms":
        logging.error("err, blaulichtsms api not yet implemented")
        code_for_screen = "alarm"
    else:
        # no proper info source, show alarm all the time
        code_for_screen = "alarm"

    # update all screens
    for screenindex in range(0, COUNT):
        SCREENSTATUS_Q_L[screenindex].put(code_for_screen)

    # schedule next update of the screens according to the CONFIGured time [S]
    threadobj = threading.Timer(CONFIG["info_source"]["service_routine_period"], update_routine)
    threadobj.start()


def closeall_webdriver(infoqueue_lst):
    """
        Send "terminate" to all browser instances to gracefully shut them down,
        kill the processed after a 5 S timeout (if the remain).

        Args
            infoqueue: message queue list for all screen processes
    """
    for infoqueue in enumerate(infoqueue_lst):
        infoqueue[1].put("terminate")
        logging.debug("sent terminate message to a screen")

    time.sleep(5)
    for proc in multiprocessing.active_children():
        proc.terminate()
        logging.debug("terminating screen forcefully")


if __name__ == '__main__':
    # initial setup of screen checker processes

    NR_SCREENS = len(CONFIG["screen"])
    STATUS_Q = []
    SCREEN_PARAM = CONFIG["screen"]
    FEEDBACK_Q = multiprocessing.Queue()
    PROCESS_Q = []
    for i in range(0, NR_SCREENS):
        STATUS_Q.append(multiprocessing.Queue())
        p = multiprocessing.Process(target=screen_unit.check_screen_p,
                                    args=(STATUS_Q[i], FEEDBACK_Q, SCREEN_PARAM[i], CONFIG))
        p.start()
        PROCESS_Q.append(p)

    SCREENSTATUS_Q_L = STATUS_Q
    COUNT = NR_SCREENS

    update_routine()

    while True:
        FEEDBACK = FEEDBACK_Q.get(True)
        if FEEDBACK == "request_termination":
            logging.warning("{} main process received termiantion request".format(os.getpid()))
            closeall_webdriver(STATUS_Q)
            os._exit(1)


################################################################################
# http server
################################################################################
#
# def print_infopage(url):
#     """
#     Print WASTL info page with detailed current status, map and nearby hydrants.
#
#     """
#
#     cups_session = cups.Connection()
#     printers = cups_session.getPrinters()
#     with open('m.txt', 'w')as output:
#         output.write('some text')
#         print ("done")  # debugging
#         prin = cups_session.getDefault()
#         output.close()
#     # add script print after close file
#     printfile = os.path.abspath("m.txt")
#     cups_session.printFile(prin, printfile, 'm.txt', {})
#
#
# def g
#
#
# @get('/map')
# def map():
#     """
#     Generate job specific map and deliver it.
#     """
#     mapparam_dict =
#     return template(CONFIG["template_standard"], mapparam_dict)
#
#
# @post('/')
# def action():
#     """
#     Receive and parse POST request.
#     Start printing info page if required
#     """
#     print ("POST received")
#     # possibly use request from bottle
#
#
# threading.Thread(target=run, kwargs=dict(host='localhost',
#                                          port=CONFIG["http_server"]["port"])).start()
