#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

"""
Helper function to fetch and parse information from https://infoscreen.florian10.info/ows/infoscreen/v3/
"""

import json
import logging

import requests


def check_wastl(wastl_cookie):
    """
    check WASTL / FF-Krems to get current status
    Args:
        wastl_cookie: access cookie to retrieve the status file
    Returns:
        status_code: classification and size in the case of an alarm (e.g. t1, s2, b4...)
            "off" in the cas of no alarm
        wastl_msg: parsed json file from wastl
    """

    WASTL_URL = "https://infoscreen.florian10.info/ows/infoscreen/einsatz.ashx?f=json"

    # get data from WASTL with authorized cookie
    try:
        wastl_session = requests.Session()
        wastl_session.mount('https://', requests.adapters.HTTPAdapter(max_retries=requests.adapters.Retry(total=5,
                                                                                                          backoff_factor=0.1, status_forcelist=[500, 502, 503, 504])))
        req = wastl_session.get(WASTL_URL, cookies={wastl_cookie[0]: wastl_cookie[1]})
        if req.status_code == 200:
            logging.debug("WASTL json: " + req.text)
            status = json.loads(req.text)
        else:
            logging.warning("WASTL returned unexpected status code (!=200): " + str(req.status_code))

        # parse data from WASTL
        if status["CurrentState"] == "data":
            if status["EinsatzData"] == []:
                return "normal", status  # idle status
            else:
                return status["EinsatzData"][0]["Alarmstufe"], status  # emergency status

        elif status["CurrentState"] == "token" or status["CurrentState"] == "waiting":
            retmsg = "WASTL Cookie not accepted, CurrentState:" + str(status["CurrentState"])
            return retmsg, status
        else:
            logging.error("unknown CurrentState from WASTL: " + str(status))
            return "error", status

    except Exception as exceptmsg:
        logging.error("exceipion while querrying WASTL: " + str(exceptmsg))
        return "exception", str(exceptmsg)
