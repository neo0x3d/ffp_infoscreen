#! /usr/bin/env python3
# -*- coding: UTF-8 -*-
"""
Generate parameters for a self programmed map.
"""

import logging

from geopy.geocoders import Nominatim


def gen_mapparam(json_parsed, highway_name, highway_km):
    """
    Generate coordinates from the adress

    Args:
        json_parsed: config from WASTL, includes address
        highway_name: string of the highways name
        highway_km: highway km with matching gps coordinates
    Returns:
        status code (0 in case of success)
        map parameters as a string (e.g. /?lat=48.2206849&lon=16.3800599&theme=fire)
    """

    try:
        # Autobahn adress (km) expected
        if highway_name in json_parsed["EinsatzData"][0]["Strasse"]:
            logging.info("A1 found in \"Strasse\" key")
            nr1 = json_parsed["EinsatzData"][0]["Nummer1"]

            # shorten km number
            if nr1.endswith(".000"):
                nr1.replace(".000", "0")
            elif nr1.endswith(".500"):
                nr1.replace(".500", "5")

            # get lat and lon from local recources

        # standard adress expected
        else:
            logging.info("A1 not found in \"Strasse\" key")
            street = json_parsed["EinsatzData"][0]["Strasse"]
            nr1 = json_parsed["EinsatzData"][0]["Nummer1"]
            nr2 = json_parsed["EinsatzData"][0]["Nummer2"]
            nr3 = json_parsed["EinsatzData"][0]["Nummer3"]
            plz = json_parsed["EinsatzData"][0]["Plz"]
            town = json_parsed["EinsatzData"][0]["Ort"]

            # shorten numbers if they end with .000
            if nr1.endswith(".000"):
                logging.debug("removing .000 from number 1")
                nr1 = nr1.replace(".000", "")
            if nr2.endswith(".000"):
                logging.debug("removing .000 from number 2")
                nr2 = nr2.replace(".000", "")
            if nr3.endswith(".000"):
                logging.debug("removing .000 from number 3")
                nr3 = nr3.replace(".000", "")

            address = plz + " " + town + ", " + \
                " ".join([street, nr1, nr2, nr3])

            logging.debug("address: " + address)

            # convert adress to coordinates
            geolocator = Nominatim()
            location = geolocator.geocode(address)

            if location is None:
                logging.error("error converting into lat/long: " + address)
                return -1, ""
            else:
                lat = location.latitude
                lon = location.longitude

    except KeyError as exceptmsg:
        logging.error("Keyerror" + str(exceptmsg))
        logging.error("json: " + str(json_parsed))

    mapparam = "/?" + "lat=" + str(lat) + "&lon=" + str(lon) + "&zoom=17"
    logging.info("generated lat/long:" + mapparam)

    return 0, mapparam
