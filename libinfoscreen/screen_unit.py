#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

"""
Spawn and manage selenium instances.
"""

import io
import logging
import os
import sys
import time
import traceback

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.command import Command


def check_screen_p(status_qeue, feedback_queue, screen_parameter, config):
    """
    Startup and control a single webdriver instance.

    Args:
        status_qeue: queue which will provide status updates for the screen,
                     triggers updates
        feedback_queue:
        screen_parameter: paramter (urls, positions, reload settings),
                          directly taken from config file
    """

    # max_status = [int max_x, int max_y, if F11 maximized]
    max_status = [0, 0, False]
    # is the cookielist loaded?
    cookie_status = [False]

    def webdriver_isalive(driver):
        """
        Check if the webdriver is still alive or already dead?

        Args:
            driver: webdriver instance
        Returns:
            bool: alive/not alive
        """
        try:
            driver.execute(Command.STATUS)
            return True
        except Exception:
            return False

    def add_cookies(driver, cookie_lst, cookie_status_lst):
        """
        Add all cookies from a list to the webdriver.
        Note: adding of cookies will lead to a reload of webpages, which may
              also be seen visually. This behaviour also happens if cookies, which
              are already in the selenium instances are loaded again (unnecessary)

        Args:
            driver: webdriver instance
            cookie_lst: list of cookie(s)

        """
        if cookie_status_lst[0] is False:
            driver.delete_all_cookies()
            # for nr in range(len(cookie_lst)):
            for nr_item, item in enumerate(cookie_lst):
                cookie = {'name': cookie_lst[nr_item][0], 'value': cookie_lst[nr_item][1]}
                logging.debug("{} adding cookie nr: {}, content: {}".format(os.getpid(), nr_item, cookie))
                driver.add_cookie(cookie)
            cookie_status_lst[0] = True
            driver.refresh()

    def checkscreen(selenium_handler, position, url, disable_urlcheck,
                    force_reload, maximized, cookies, cookie_stat_lst,
                    use_geko_log_path):
        """
        Check if the webdriver is workig as supposed
                    (url, position, size/fullscreen)

        Args:
            selenium_handler: webdriver instance
            position: x, y pixel coordinates
            url: url to display
            force_reload: url should be reloaded
            use_geko_log_path: (bool) use a specific log path for
                                the geckodriver(needed on Cenot 7,
                                not needed on Fedora 25)
        """
        # try:
        if not webdriver_isalive(selenium_handler[0]):
            logging.warning("{} webdriver not alive, hard reset".format(os.getpid()))
            try:  # try to quit the webdriver, may fail if the instance is already dead
                maximized[2] = False
                cookie_stat_lst[0] = False
                selenium_handler[0].quit()
            except Exception:
                pass
            if use_geko_log_path:
                selenium_handler[0] = webdriver.Firefox(log_path=os.path.join(
                    config["log"]["path"], "geckodriver_{}.log".format(os.getpid())))
            else:
                selenium_handler[0] = webdriver.Firefox()

        # check url if needed
        do_reload = False
        if disable_urlcheck != True:
            cur_url = selenium_handler[0].current_url
            if url != cur_url:
                do_reload = True

        # force reload if needed
        if force_reload:
            do_reload = True

        if do_reload:
            logging.debug("{} reloading/changing url(could be initial start): {}".format(os.getpid(), url))
            selenium_handler[0].get(url)
            cookie_stat_lst[0] = False
            add_cookies(selenium_handler[0], cookies, cookie_stat_lst)

        # check if position has changed
        # get_window_position may fail (return NoneType) with geckodriver < V0.15 !

        # currently a bit hacky, should be converted from tuple to list
        position = (position[0], position[1])

        currentpos = selenium_handler[0].get_window_position()
        if (currentpos['x'], currentpos['y']) != position:
            if (0, 0) == position:
                if not ((position[0] + 60 <= currentpos['x']) or (position[1] + 60 <= currentpos['y'])):
                    # it is expected that the first screen (with coordinates 0,0) has a task bar at the top or
                    # left side and thus the window will be moved away from (0,0)
                    logging.debug("{} main screen  (probably taskbar) not changing position".format(os.getpid()))
                else:
                    logging.debug("{} main screen position off, changing position from: {}, to: {}".format(
                        os.getpid(), str(currentpos), str(position)))
                    selenium_handler[0].set_window_position(position[0], position[1])
            else:
                logging.debug("{} changing position from: {}, to: {}".format(
                    os.getpid(), str(currentpos), str(position)))
                selenium_handler[0].set_window_position(position[0], position[1])

        # check if maximized
        current_size = selenium_handler[0].get_window_size()
        if not (current_size["width"] == maximized[0] and current_size["height"] == maximized[1]):
            logging.debug("{} current size does not match previous maximized size x: {} y:{} , maximizing".format(
                os.getpid(), maximized[0], maximized[1]))
            selenium_handler[0].maximize_window()
            maximized[2] = False

        # check if F11 fullscreen
        if not maximized[2]:
            logging.debug("{} emulating f11 after 5S delay".format(os.getpid()))
            # it is ugly but seems necessary to work less buggy
            # problems with 1 S delay have been noticed on modern hardware, increased to 5 S
            time.sleep(5)
            selenium_handler[0].find_element_by_tag_name("html").send_keys(Keys.F11)
            maximized[2] = True

        # update maximized size (wait 1 second or size may be not correct, yes it is ugly)
        time.sleep(5)
        current_size = selenium_handler[0].get_window_size()
        maximized[0] = current_size["width"]
        maximized[1] = current_size["height"]

    # entry point for checkscreen(), initial start of the webdriver instance
    try:
        if screen_parameter["geckodriver_explicit_log_path"]:
            sel_instance = [webdriver.Firefox(log_path=os.path.join(
                config["log"]["path"], "geckodriver_{}.log".format(os.getpid())))]
        else:
            sel_instance = [webdriver.Firefox()]
    except Exception as exceptmsg:
        logging.error("{} error at initial start of webdriver: {} ".format(os.getpid(), exceptmsg))
        feedback_queue.put("request_termination")

    reload_counter = 1  # set to 1, so a periodic_reload=1 will result in a reload every time

    # main routine (wraped in try statement to catch traceback)
    while True:
        try:
            current_status = status_qeue.get(True)

            # parse received status info, set url acordingly
            if current_status == "normal":
                url = screen_parameter["normal"]["url"]
                no_urlcheck = screen_parameter["normal"]["disable_urlcheck"]
                cookie_list = screen_parameter["normal"]["cookie_list"]
            elif current_status == "alarm":
                url = screen_parameter["alarm"]["url"]
                no_urlcheck = screen_parameter["alarm"]["disable_urlcheck"]
                cookie_list = screen_parameter["alarm"]["cookie_list"]
            # elif current_status == "terminate":
            # unknown status or terminate status from main proc, shut down
            else:
                try:
                    sel_instance[0].quit()
                except Exception:
                    logging.debug("{} screen process exits NOW".format(os.getpid()))
                    sys.exit(0)

            reload_en = False
            if screen_parameter["periodic_reload"] > 0:  # disable at 0
                if reload_counter >= screen_parameter["periodic_reload"]:
                    reload_en = True
                    reload_counter = 1
                else:
                    reload_counter += 1

            # do the needful to keep the screen happy :)
            checkscreen(sel_instance, screen_parameter["pos_xy"],
                        url, no_urlcheck,
                        reload_en, max_status, cookie_list, cookie_status,
                        screen_parameter["geckodriver_explicit_log_path"])
            reload_en = False

        except Exception as e:
            logging.error("{} exception while checking screen, request termination, msg was: {}".format(os.getpid(), e))

            # log traceback/exception (https://seasonofcode.com/posts/python-multiprocessing-and-exceptions.html)
            exc_buffer = io.StringIO()
            traceback.print_exc(file=exc_buffer)
            logging.error('Uncaught exception in worker process:\n%s', exc_buffer.getvalue())

            feedback_queue.put("request_termination")
