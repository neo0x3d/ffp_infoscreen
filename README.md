# FFP_Infoscreen

Display information for the voluntary fire department, using WASTL.


## Prepare Host System

Host specific packages, changes.

CentOS 7: [here](CentOS7_setup.md)

Planned for the future.

Raspbian: [here](Raspbian_setup.md)

## Download and Installation/Setup

**Download ffp_infoscreen**

Clone the github repo to the previously created users home folder.

```
$ git clone https://gitlab.com/neo0x3d/ffp_infoscreen.git
```

**Install Python3 dependencies**

```
$ sudo pip3 install -r requirements.txt
```

**Setup (short version)**

Visit <https://infoscreen.florian10.info/ows/infoscreen/v3/> with Firefox and enable the displayed token in the admin interface to get an access cookie (this cookie is needed for further operation, do not delete it).

- ffp_infoscreen.py

  - Change path to the json config file (absolute path)

- ffp_infoscreen.json

  - Edit the sections for the screens.
  - Add the cookie (name and value) from Firefox to the wastl section, for each screen containing the infoscreen url and printing page.
  - Change log folder path.

  For a more detailed documentation look at [config_file_documentation.md](config_file_documentation.md)

**Add systemd unit and start**

Systemd is used to keep the script alive and restart if it crashes.

Before using Systemd, is recommended to test the setup manually and check if be behavior matches the expectations and no errors occour:

```
$ ./ffp_infoscreen.py
```

Set the path to the python script in the ffp_infoscreen@.service file, then copy it to the Systemds system folder, enable and start under the user selected before!

```
$ sudo cp ffp_infoscreen@.service /etc/systemd/system
$ sudo systemctl enable ffp_infoscreen@user.service
$ sudo systemctl start ffp_infoscreen@user.service
```

## Apendix


**Known Bugs/Problems**

- 09.04.2017: CentOS 7 & Firefox 52.0: Selenium get_window_position() function in Python3 returns NoneType, results in Firefox window opening and closing immediately, use Geckodriver V0.15 (or higher)
