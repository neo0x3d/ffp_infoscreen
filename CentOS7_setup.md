# Installation of CentOS 7

General purpose CentOS 7 installation with basic security measures and additional setup for:

- ffp_infoscreen (this repo)
- ffp_fotoupload [ffp_fotoupload](https://github.com/neo0x3d/ffp_fotoupload)

**Basic Installation**

CentOS 7 can be setup headless (without X11 and DE), but since this computer will also be used as information display the setup includes the GNOME desktop.

1. Obtain the installation media from <https://www.centos.org/download/>, verify checksum afterwards.
2. Create a bootable media via dd or <https://rufus.akeo.ie/> on Windows
3. Boot with the "Test this media & install CentOS 7" option (default)
4. Select language & Keyboard
5. Install with following options:

  - Software Selection -> Minimal Install -> Security Tools
  - Software Selection -> Minimal Install -> Smart Card Support
  - Software Selection -> Compute Node -> Hardware Monitoring Utilities
  - Software Selection -> Compute Node -> Network File System Client
  - Software Selection -> Infrastructure Server -> FTP Server
  - Software Selection -> Infrastructure Server -> E-mail Server
  - Software Selection -> Infrastructure Server -> Remote Management for Linux
  - Software Selection -> GNOME Desktop -> GNOME Applications
  - Software Selection -> GNOME Desktop -> Internet Applications
  - Software Selection -> Development and Creative Workstation -> Python
  - Software Selection -> select "Server with GUI" click Done
  - Set up automatic partitioning
  - Enable Network connection
  - Security Policy -> Common Profile for General-Purpose systems

Set the Hostname in the Network settings to something usable.

Update all system packages after the fresh installation

**Setup root mail forwarding and test it (requires running MDA, e.g. Postfix)**

/etc/aliases (at the bottom)

```
root: user@domain.tld
```

Test the forwarding

```
$ sudo newaliases
$ echo test | mail -s "test message" root
```

**Harden SSH access**

/etc/ssh/sshd_config (only changes are listed)

```
Port somerandomport
Protocol 2
AllowUsers user1 user2
PermitRootLogin no
StrictModes yes
PermitEmptyPasswords no
PasswordAuthentication no
PubkeyAuthentication yes
AuthorizedKeysFile      .ssh/authorized_keys
RhostsRSAAuthentication no
UsePAM yes
ClientAliveInterval 600
ClientAliveCountMax 3
```

And install fail2ban to automatically ban IPs after failed login attempts.

```
$ sudo yum install fail2ban
```

/etc/fail2ban/jail.conf

```
[DEFAULT]
# Ban hosts for one hour:
bantime = 3600

# Override /etc/fail2ban/jail.d/00-firewalld.conf:
banaction = iptables-multiport

[sshd]
enabled = true
```

```
$ sudo systemctl enable fail2ban
$ sudo systemctl start fail2ban
```

**Switch the system log to persistent**

Switch journald to persistant logging (logs will survive reboots). [Further information at digitalocean](https://www.digitalocean.com/community/tutorials/how-to-use-journalctl-to-view-and-manipulate-systemd-logs)

/etc/systemd/journald.conf

```
Storage=persistent
```

**Set up static IP**

## Specific for FFP_Infoscreen

- Add a user with minimal permissions, who will be automatically logged in (will run the infoscreen script)
- Disable screen saver and screen lock for this user

**Install Python3, PIP3 and Geckodriver**

CentOS 7 does neither come with Python3 nor pip3\. Install the rpmfusion free first <https://rpmfusion.org/Configuration/> and then the Python components.

```
$ sudo yum install python34 python34-pip
```

Selenium need the Geckodriver to interact with Firefox. Download it and add the binary to the path of the root user (or copy the binary to e.g. /usr/bin/) <https://github.com/mozilla/geckodriver/releases>

When starting via systemd the geckodriver binary has to be present somewhere in the $PATH variable of the root user.
