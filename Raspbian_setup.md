# Installation on Raspbian/Raspberry Pi

This

- ffp_infoscreen (this repo)
- ffp_fotoupload [ffp_fotoupload](https://github.com/neo0x3d/ffp_fotoupload)

The installation can be done entirely without keyboard and mouse (over ssh), but also with a keyboard and mouse directly attached to the Raspberry.

**Hardware**

This guide/application was designed to work on the recent hardware versions (2, 3, Zero and below will not work through missing hardware floating point support, which is required by Geckodriver). HDMI to VGA adapters were avoided but may function.

**Download and write to SD card**

Download the most recent version of Raspbian Jessi with Pixel here: <https://www.raspberrypi.org/downloads/>

Write it to the SD card via dd on Linux or Rufus on Windows <https://rufus.akeo.ie>

On the first start update all packages.

**Setup and Harden SSH access**

On all recent versions of Raspbian the SSH Server is disabled by default. SSH might be used to configure the program and update all packages.

To set up ssh: <https://www.raspberrypi.org/documentation/remote-access/ssh/>

**_First SSH Login and changing password_**

Login via SSH (user pi, password raspberry) and change the default password as soon as possible!

```
$ ssh pi@192.168.1.15
.
.
passwd
```

**_Enable Private/Public Key Authentication_**

Using Key files instead of Passwords will be more secure while offering more user comfort. For more information read on the official Raspberry Page <https://www.raspberrypi.org/documentation/remote-access/ssh/passwordless.md>

Modify the sshd_config, to that the ssh server only accept the key file and no password any more. /etc/ssh/sshd_config (only changes are listed)

```
Port somerandomport
Protocol 2
AllowUsers user1 user2
PermitRootLogin no
StrictModes yes
PermitEmptyPasswords no
PasswordAuthentication no
PubkeyAuthentication yes
AuthorizedKeysFile .ssh/authorized_keys
RhostsRSAAuthentication no
UsePAM yes
ClientAliveInterval 600
ClientAliveCountMax 3
```

## Set up static IP

To ease finding the Raspberry over the network, a static IP can be used.

/etc/dhcpcd.conf (Debian Jessie with running dhcpcd)

```
interface eth0
static ip_address=192.168.1.2/24
static routers=192.168.1.1
static domain_name_servers=192.168.1.1
```

If dhcpcd is NOT running, the interfaces file could be modified instead of the dhcpcd config.<br>
/etc/network/interfaces

```
# Ethernet
auto eth0
allow-hotplug eth0
iface eth0 inet static
address 192.168.1.2
netmask 255.255.255.0
gateway 192.168.1.1
dns-nameservers 192.168.1.1
```

**Change Hostname**

```
$ sudo hostnamectl set-hostname somethingmemorable
```

**Mail Delivery Agent (Postfix)**

Install Postfix and mailutils (provides mail)

```
$ sudo apt-get install postfix mailutils
```

- Use "Internet Site" since this will only be used to send notifications.
- Leave the mail name set as default (raspberrypi)
- Set the Root and Postmaster recipient to the desired email adress.
- Leave the accpeting domains the same. Use forces synchronus queue processing.
- Network settings stay unchanged (default local host)
- No mailbox limit size (default)
- Local adress extension stays unchanged (default)
- Set to only use IPv4

```
$ echo test | mail -s "test message" root
```

## Specific for FFP_Infoscreen

**Install Firefox and Geckodriver**

```
$ sudo apt-get install firefox-esr
```

Selenium need the Geckodriver to interact with Firefox. Download it and add the binary to the path of the root user (or copy the binary to e.g. /usr/bin/) <https://github.com/mozilla/geckodriver/releases>
