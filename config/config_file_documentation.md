# Config File Documentation

## "log" -> logging parameters

## "info_source" -> information trigger source.

**service_routine_period** defines the time in seconds on which the information source will be querried. (this triggers the update of all screens)

**info_source_to_use** Possible: "wastl", "blaulichtsms", "none"
wastl may take 5-10 minutes to be up to date while blaulichtsms is instant. wastl also provides a proper end of the emergencies while blaulichtsms will never be able to tell the actual end(this is how their systems are set up)
none will always return alarm mode (if neither blaulichtsms nor wastl should be used)

## "screen" -> defines each physical screen

The URL defined here will be opened in an isolated Firefox window and expanded to the phsysical size of the screen (like F11).
Depending on the current mode (possible are "normal" and "alarm") the appropriate url will be loaded as well as the list of applicable cookies.

**alarm, normal** sections hold separate information for both modi.

**url** defines the url auf the target site

**disable_urlcheck** disables the url check on each update, can be used if the site uses an automatic redirection which would result in a visible reload.

**cookie_list** list of sublists. Each sublist holds the cookie name and cookie value. These will be loaded upon webbrowser (re)start.

**pos_xy** (list, unit is pixel) has to be anywhere on the target screen, window will be moved here and expand to fullscreen automatically and fit to the physical size.

If the url does not match the set one, it will be automatically changed. (url check can be disabled and or a list of tollerable url changes can be used to cope with sites redirecting automatically)

**periodic_reload** disabled if set to 0, if set to 1 reloads every time, if set to higher than 1 it will reload the url every x iteration.

**geckodriver_explicit_log_path** might be needed on CentOS only, points to a log directory.

Example configuration for two screens may look as following
```
```
## "http_server"

This section defines the webui.
